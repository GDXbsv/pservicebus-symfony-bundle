<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\DependencyInjection\Compiler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceInit;
use GDXbsv\PServiceBus\Doctrine\DbalOnlyOnceControl;
use GDXbsv\PServiceBus\Doctrine\DoctrineInMiddleware;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrineOutboxInitConsoleCommand;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrinePersistence;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrineSubscriber;
use GDXbsv\PServiceBus\Saga\SagaPersistence;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

final class DoctrinePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $pingTimeout = $container->getParameter('p-service_bus.temp.doctrine.ping_timeout');
        $container->getParameterBag()->remove('p-service_bus.temp.doctrine.ping_timeout');
        $rollbackTransaction = $container->getParameter('p-service_bus.temp.doctrine.rollback_transaction');
        $container->getParameterBag()->remove('p-service_bus.temp.doctrine.rollback_transaction');
        if (!$container->has(EntityManagerInterface::class)) {
            return;
        }
        $definition = new Definition(SagaDoctrinePersistence::class);
        $definition->addMethodCall('setCoroutineBus', [new Reference(CoroutineBus::class)]);
        $definition->setArgument('$tableOutbox', 'messages_to_send');
        $definition->setAutowired(true)->setAutoconfigured(true);
        $container->setDefinition(SagaDoctrinePersistence::class, $definition);
        $container->setAlias(SagaPersistence::class, SagaDoctrinePersistence::class);

        $definition = new Definition(SagaDoctrineOutboxInitConsoleCommand::class);
        $definition->setArgument('$tableOutbox', 'messages_to_send');
        $definition->setAutowired(true)->setAutoconfigured(true);
        $container->setDefinition(SagaDoctrineOutboxInitConsoleCommand::class, $definition);


        $definition = new Definition(SagaDoctrineSubscriber::class);
        $definition->addTag('doctrine.orm.entity_listener', [
            // These are the options required to define the entity listener:
            'event' => Events::loadClassMetadata,
            // 'entity' => User::class,

            // These are other options that you may define if needed:

            // set the 'lazy' option to TRUE to only instantiate listeners when they are used
            // 'lazy' => true,

            // set the 'entity_manager' option if the listener is not associated to the default manager
            // 'entity_manager' => 'custom',

            // by default, Symfony looks for a method called after the event (e.g. postUpdate())
            // if it doesn't exist, it tries to execute the '__invoke()' method, but you can
            // configure a custom method name with the 'method' option
            'method' => 'loadClassMetadata',
        ]);
        $definition->setAutowired(true)->setAutoconfigured(true);
        $container->setDefinition(SagaDoctrineSubscriber::class, $definition);

        $definition = new Definition(DbalOnlyOnceControl::class);
        $definition->setArgument('$tableName', 'only_once');
        $definition->setAutowired(true)->setAutoconfigured(true);
        $container->setDefinition(DbalOnlyOnceControl::class, $definition);
        $container->setAlias(OnlyOnceControl::class, DbalOnlyOnceControl::class);
        $container->setAlias(OnlyOnceInit::class, DbalOnlyOnceControl::class);

        $definition = new Definition(DoctrineInMiddleware::class);
        $definition
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setArgument('$pingTimeout', $pingTimeout)
            ->setArgument('$rollbackTransaction', $rollbackTransaction);
        $container->setDefinition(DoctrineInMiddleware::class, $definition);
    }
}
