<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundleTests\Integration;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBusBundleTestApp\InMemoryTraceTransport;
use GDXbsv\PServiceBusBundleTestApp\Message;

final class MultiTransportsRegressionTest extends FunctionalTestCase
{
    public function testMulti(): void
    {
        /** @var Bus $bus */
        $bus = self::getContainer()->get(Bus::class);
        $bus->publish(new Message());
        /** @var InMemoryTraceTransport $transport1 */
        $transport1 = self::getContainer()->get('InMemory1');
        /** @var InMemoryTraceTransport $transport2 */
        $transport2 = self::getContainer()->get('InMemory2');
        $this->assertEquals('InMemory1', $transport1->name);
        $this->assertEquals('InMemory2', $transport2->name);
        $this->assertCount(1, $transport1->envelopesRecorded);
        $this->assertCount(1, $transport2->envelopesRecorded);

    }
}
