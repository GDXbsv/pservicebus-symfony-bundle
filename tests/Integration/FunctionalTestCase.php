<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusBundleTests\Integration;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class FunctionalTestCase extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        static::bootKernel();
        $this->init();
    }

    protected function tearDown(): void
    {
    }

    protected function init()
    {
        $application = new Application(self::$kernel);

        $command       = $application->find('p-service-bus:init');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        static::assertSame(0, $commandExitCode);
    }

    protected function consume(string $from): int
    {
        $application = new Application(self::$kernel);

        $command       = $application->find('p-service-bus:transport:consume');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'command'   => $command->getName(),
                'transport' => $from,
                '--limit'   => 50,
            ]
        );

        return $commandExitCode;
    }
}
