# PServiceBus Symfony Bundle

Telegram group: https://t.me/PServiceBus

For library https://gitlab.com/GDXbsv/pservicebus

Symfony: https://packagist.org/packages/gdx/p-service-bus-symfony-bundle

Laravel: https://packagist.org/packages/gdx/p-service-bus-laravel-package


As example for config please look in TestApp folder: 
https://gitlab.com/GDXbsv/pservicebus-symfony-bundle/-/tree/master/TestApp


Example initialization file `config/packages/p-service-bus.php`:
```php
<?php declare(strict_types=1);

use GDXbsv\PServiceBus\Bus\ConsumeBus;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBus\Transport\Sns\SnsSqsTransport;
use GDXbsv\PServiceBus\Transport\Sqs\SqsTransport;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\inline_service;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension(
        'p_service_bus',
        [
            'transports' => [
                'memory' => InMemoryTransport::class,
                'external' => SnsSqsTransport::class,
                'example_name_for_transport' => 'app.service_bus.transport.example_sqs',
                'example_name_for_transport2' => 'app.service_bus.transport.example_sqs2',
            ],
        ]
    );

    $s = $containerConfigurator->services();

    $s->defaults()
        ->autowire()
        ->autoconfigure();

    $s->set(InMemoryTransport::class)
        ->call('setBus', [service(ConsumeBus::class)]);
    $s
        ->set(SnsSqsTransport::class)
        ->factory([SnsSqsTransport::class, 'ofDsn'])
        ->arg('$dsnString', '%env(SNS_DSN)%')
        ->arg('$sqsTransport', inline_service(SqsTransport::class)
            ->factory([SqsTransport::class, 'ofDsn'])
            ->arg('$dsnString', '%env(SQS_DSN)%&queue=core_external')
        );
    $s
        ->set('app.service_bus.transport.example_sqs')
        ->class(SqsTransport::class)
        ->factory([SqsTransport::class, 'ofDsn'])
        ->arg('$dsnString', '%env(SQS_DSN)%&queue=core_example_name');
    $s
        ->set('app.service_bus.transport.example_sqs2')
        ->class(SqsTransport::class)
        ->factory([SqsTransport::class, 'ofDsn'])
        ->arg('$dsnString', '%env(SQS_DSN)%&queue=core_example_name2');
```
