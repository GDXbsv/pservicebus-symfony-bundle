<?php

declare(strict_types=1);

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\ConsumeBus;
use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\Bus\TraceableBus;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBusBundleTestApp\InMemoryTraceTransport;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services
        ->load('GDXbsv\\PServiceBusTestApp\\', __DIR__ . '/../../vendor/gdx/p-service-bus/TestApp')
        ->exclude(
            [
                __DIR__ . '/../../vendor/gdx/p-service-bus/TestApp/bootstrap.php',
                __DIR__ . '/../../vendor/gdx/p-service-bus/TestApp/Saga/CustomDoctrineSagaFinder.php'
            ]
        );

    $services
        ->load('GDXbsv\\PServiceBusBundleTestApp\\', __DIR__ . '/../src');

    $services->set(InMemoryTransport::class);
    $services->set('InMemory1')
        ->class(InMemoryTraceTransport::class)
        ->call('setBus', [service(ServiceBus::class)])
        ->property('name', 'InMemory1');
    $services->set('InMemory2')
        ->class(InMemoryTraceTransport::class)
        ->call('setBus', [service(ServiceBus::class)])
        ->property('name', 'InMemory2');
    $services->set(TraceableBus::class)
        ->decorate(Bus::class)
        ->decorate(CoroutineBus::class)
        ->decorate(ConsumeBus::class)
        ->args(
            [
                service(ServiceBus::class),
                service(ServiceBus::class),
                service(ServiceBus::class),
            ]
        );
};


